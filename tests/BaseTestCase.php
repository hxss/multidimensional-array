<?php

use MultidimensionalArray\Dimension;
use MultidimensionalArray\MultidimensionalArray;
use PHPUnit\Framework\TestCase;

class BaseTestCase extends TestCase {

	protected function isPhp7() {
		return phpversion() > 6;
	}

	protected function getCustomTestCase() {
		return new CustomGeneratorTestCase($this->getTestArray());
	}

	protected function getTestArray() {
		return new MultidimensionalArray([
			0 => [
				'name' => 'name1',
				'prop' => 'prop1',
				'key' => 'value1'
			],
			1 => [
				'name' => 'name5',
				'prop' => 'prop5',
				'key' => 'value5'
			],
			2 => [
				'name' => 'name2',
				'prop' => 'prop2',
				'key' => 'value2'
			],
			'a' => [
				'name' => 'name3',
				'prop' => 'prop3',
				'key' => 'value3'
			],
			'b' => [
				'name' => 'name4',
				'prop' => 'prop4',
				'key' => 'value4'
			],
			'c' => [
				'name' => 'name6',
				'prop' => 'prop5',
				'key' => 'value6'
			],
			'd' => [
				'name' => 'name7',
				'prop' => 'prop7',
				'key' => 'value7'
			],
		]);
	}

	protected function getValidAnswer() {
		return [
			'prop1' => [
				'name' => 'name1',
				'prop' => 'prop1',
				'key' => 'value1'
			],
			'prop5' => [
				'name' => 'name6',
				'prop' => 'prop5',
				'key' => 'value6'
			],
			'prop2' => [
				'name' => 'name2',
				'prop' => 'prop2',
				'key' => 'value2'
			],
			'prop3' => [
				'name' => 'name3',
				'prop' => 'prop3',
				'key' => 'value3'
			],
			'prop4' => [
				'name' => 'name4',
				'prop' => 'prop4',
				'key' => 'value4'
			],
			'prop7' => [
				'name' => 'name7',
				'prop' => 'prop7',
				'key' => 'value7'
			],
		];
	}
}

class CustomGeneratorTestCase {
	public $array = null;
	public $counter = 0;
	public $dimension = null;

	public function __construct($array) {
		$this->array = $array;
		$this->dimension = new Dimension('cstmProp', function($k, $v) {
			$this->counter++;

			return $v['prop'];
		});
		$this->dimension->setMaster($this->array);
	}
}

class TestObject1 {

	private $prop = null;

	public function __construct($prop) {
		$this->prop = $prop;
	}

	public function prop() {
		return $this->prop;
	}
}

class TestObject2 {

	private $prop = null;

	public function __construct($prop) {
		$this->prop = $prop;
	}

	public function getProp() {
		return $this->prop;
	}
}
