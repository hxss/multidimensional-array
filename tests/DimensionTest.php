<?php

include_once 'BaseTestCase.php';

use ArrayObject\ArrayObject;
use MultidimensionalArray\Dimension;

class DimensionTest extends BaseTestCase {

	public function testDefaultDimensionGeneratorException() {
		$this->expectException(\Exception::class);

		$o = ArrayObject::create([1, 2]);
		$dimension = new Dimension('prop');
		$dimension->setMaster($o);
	}

	/**
	 * @depends testDefaultDimensionGeneratorException
	 */
	public function testDefaultDimensionGenerator() {
		$o = $this->getTestArray();
		$dimension = new Dimension('prop');

		$this->assertSame('prop', $dimension->name);
		$this->assertTrue($dimension->array->eq([]));

		$dimension->setMaster($o);

		$this->assertSame(
			$this->getValidAnswer(),
			$dimension->array->items()
		);

		$o = $this->getTestArray();
		$o->walk(function(&$v) {
			$v = (object)$v;
		});
		$dimension = new Dimension('prop');
		$dimension->setMaster($o);
		$this->assertSame(
			$this->getValidAnswer(),
			$dimension->array->walk(function(&$v) {
				$v = (array)$v;
			})->items()
		);

		$p3 = new TestObject1('prop3');
		$p4 = new TestObject1('prop4');
		$o = ArrayObject::create([$p3, $p4]);
		$dimension = new Dimension('prop');
		$dimension->setMaster($o);
		$this->assertSame(
			[
				'prop3' => $p3,
				'prop4' => $p4,
			],
			$dimension->array->items()
		);

		$p3 = new TestObject2('prop3');
		$p4 = new TestObject2('prop4');
		$o = ArrayObject::create([$p3, $p4]);
		$dimension = new Dimension('prop');
		$dimension->setMaster($o);
		$this->assertSame(
			[
				'prop3' => $p3,
				'prop4' => $p4,
			],
			$dimension->array->items()
		);
	}

	/**
	 * @depends testDefaultDimensionGenerator
	 */
	public function testCustomDimensionGenerator() {
		$testCase = $this->getCustomTestCase();

		$this->assertSame(
			$this->getValidAnswer(),
			$testCase->dimension->array->items()
		);

		$this->assertSame(7, $testCase->counter);
	}

	/**
	 * @depends testCustomDimensionGenerator
	 */
	public function testOnUpdateEvent() {
		$testCase = $this->getCustomTestCase();

		$testCase->counter = 0;
		$testCase->array->recursive();
		$this->assertSame(7, $testCase->counter);
		$this->assertSame(
			$this->getValidAnswer(),
			$testCase->dimension->array->items(true)
		);

		$testCase->counter = 0;
		$testCase->array->walk(function() {});
		$this->assertSame(7, $testCase->counter);
		$this->assertSame(
			$this->getValidAnswer(),
			$testCase->dimension->array->items(true)
		);
	}

	/**
	 * @depends testCustomDimensionGenerator
	 */
	public function testOnItemAdded() {
		$testCase = $this->getCustomTestCase();
		$testCase->counter = 0;
		$testCase->array->push(['prop' => 'prop6']);
		$this->assertSame(1, $testCase->counter);
		$this->assertSame(
			$this->getValidAnswer() + ['prop6' => ['prop' => 'prop6']],
			$testCase->dimension->array->items()
		);

		$testCase = $this->getCustomTestCase();
		$testCase->counter = 0;
		$testCase->array->unshift(['prop' => 'prop6']);
		$this->assertSame(1, $testCase->counter);
		$this->assertSame(
			$this->getValidAnswer() + ['prop6' => ['prop' => 'prop6']],
			$testCase->dimension->array->items()
		);

		$testCase = $this->getCustomTestCase();
		$testCase->counter = 0;
		$testCase->array[] = ['prop' => 'prop6'];
		$this->assertSame(1, $testCase->counter);
		$this->assertSame(
			$this->getValidAnswer() + ['prop6' => ['prop' => 'prop6']],
			$testCase->dimension->array->items()
		);

		$testCase = $this->getCustomTestCase();
		$testCase->counter = 0;
		$testCase->array['newKey'] = ['prop' => 'prop6'];
		$this->assertSame(1, $testCase->counter);
		$this->assertSame(
			$this->getValidAnswer() + ['prop6' => ['prop' => 'prop6']],
			$testCase->dimension->array->items()
		);
	}

	/**
	 * @depends testCustomDimensionGenerator
	 */
	public function testOnItemRemoved() {
		$testCase = $this->getCustomTestCase();
		$testCase->counter = 0;
		$testCase->array->pop();
		$this->assertSame(1, $testCase->counter);
		$validAnswer = $this->getValidAnswer();
		unset($validAnswer['prop7']);
		$this->assertSame(
			$validAnswer,
			$testCase->dimension->array->items()
		);

		$testCase = $this->getCustomTestCase();
		$testCase->counter = 0;
		$testCase->array->shift();
		$this->assertSame(1, $testCase->counter);
		$validAnswer = $this->getValidAnswer();
		unset($validAnswer['prop1']);
		$this->assertSame(
			$validAnswer,
			$testCase->dimension->array->items()
		);

		$testCase = $this->getCustomTestCase();
		$testCase->counter = 0;
		unset($testCase->array[2]);
		$this->assertSame(1, $testCase->counter);
		$validAnswer = $this->getValidAnswer();
		unset($validAnswer['prop2']);
		$this->assertSame(
			$validAnswer,
			$testCase->dimension->array->items()
		);
	}

	/**
	 * @depends testOnItemRemoved
	 */
	public function testCollisions() {
		$testCase = $this->getCustomTestCase();
		$testCase->counter = 0;
		$testCase->array->pop();
		$this->assertSame(1, $testCase->counter);
		$validAnswer = $this->getValidAnswer();
		unset($validAnswer['prop7']);
		$this->assertSame(
			$validAnswer,
			$testCase->dimension->array->items()
		);

		$testCase->counter = 0;
		$testCase->array->pop();
		$this->assertSame(1, $testCase->counter);
		$validAnswer['prop5'] = [
			'name' => 'name5',
			'prop' => 'prop5',
			'key' => 'value5'
		];
		$this->assertSame(
			$validAnswer,
			$testCase->dimension->array->items()
		);

		$testCase->counter = 0;
		$testCase->array->push([
			'name' => 'name6',
			'prop' => 'prop5',
			'key' => 'value6'
		]);
		$this->assertSame(1, $testCase->counter);
		$validAnswer['prop5'] = [
			'name' => 'name6',
			'prop' => 'prop5',
			'key' => 'value6'
		];
		$this->assertSame(
			$validAnswer,
			$testCase->dimension->array->items()
		);

		$testCase->counter = 0;
		$testCase->array->pop();
		$this->assertSame(1, $testCase->counter);
		$validAnswer['prop5'] = [
			'name' => 'name5',
			'prop' => 'prop5',
			'key' => 'value5'
		];
		$this->assertSame(
			$validAnswer,
			$testCase->dimension->array->items()
		);

		$testCase->counter = 0;
		unset($testCase->array[1]);
		$this->assertSame(1, $testCase->counter);
		unset($validAnswer['prop5']);
		$this->assertSame(
			$validAnswer,
			$testCase->dimension->array->items()
		);

		$testCase->counter = 0;
		$testCase->array->push([
			'name' => 'name6',
			'prop' => 'prop5',
			'key' => 'value6'
		]);
		$this->assertSame(1, $testCase->counter);
		$validAnswer['prop5'] = [
			'name' => 'name6',
			'prop' => 'prop5',
			'key' => 'value6'
		];
		$this->assertSame(
			$validAnswer,
			$testCase->dimension->array->items()
		);

		$testCase->counter = 0;
		$testCase->array->push([
			'name' => 'name5',
			'prop' => 'prop5',
			'key' => 'value5'
		]);
		$this->assertSame(1, $testCase->counter);
		$validAnswer['prop5'] = [
			'name' => 'name5',
			'prop' => 'prop5',
			'key' => 'value5'
		];
		$this->assertSame(
			$validAnswer,
			$testCase->dimension->array->items()
		);
	}

	/**
	 * @depends testCustomDimensionGenerator
	 */
	public function testOnSort() {
		$testCase = $this->getCustomTestCase();
		$testCase->counter = 0;
		$testCase->array->shuffle();
		$testCase->array->sort();
		$testCase->array->multisort();
		$this->assertSame(0, $testCase->counter);
		$this->assertSame(
			$this->getValidAnswer(),
			$testCase->dimension->array->items(true)
		);

		$testCase->dimension->setUpdateOnSort(true);
		$this->assertSame(7, $testCase->counter);
		$this->assertSame(
			ArrayObject::create($this->getValidAnswer())
				->sort(ArrayObject::FLAG_KEY)
				->items(),
			$testCase->dimension->array->items(true)
		);

		$testCase->counter = 0;
		$testCase->array->shuffle();
		$testCase->array->sort();
		$testCase->array->multisort();
		$this->assertSame(7*3, $testCase->counter);
		$this->assertSame(
			ArrayObject::create($this->getValidAnswer())
				->sort(ArrayObject::FLAG_KEY)
				->items(),
			$testCase->dimension->array->items(true)
		);

		$testCase->counter = 0;
		$testCase->dimension->setUpdateOnSort(true);
		$this->assertSame(0, $testCase->counter);
		$testCase->dimension->setUpdateOnSort(false);
		$this->assertSame(0, $testCase->counter);
		$testCase->array->shuffle();
		$testCase->array->sort();
		$testCase->array->multisort();
		$this->assertSame(0, $testCase->counter);
	}

	/**
	 * @depends testOnUpdateEvent
	 */
	public function testSetMaster() {
		$testCase = $this->getCustomTestCase();
		$testCase->counter = 0;

		$o = $this->getTestArray();
		$testCase->dimension->setMaster($o);
		$testCase->array->recursive();
		$this->assertSame(7, $testCase->counter);
		$this->assertSame(
			$this->getValidAnswer(),
			$testCase->dimension->array->items()
		);
	}

	/**
	 * @depends testCustomDimensionGenerator
	 * @depends testOnUpdateEvent
	 * @depends testOnItemRemoved
	 * @depends testOnSort
	 */
	public function testUnsetMaster() {
		$testCase = $this->getCustomTestCase();
		$testCase->counter = 0;
		$testCase->dimension->unsetMaster();

		$testCase->array->recursive();
		$testCase->array->push(['prop' => 'prop6']);
		$testCase->array->shift();
		$testCase->array->shuffle();
		$this->assertSame(0, $testCase->counter);
		$this->assertSame(
			[],
			$testCase->dimension->array->items()
		);
	}
}
