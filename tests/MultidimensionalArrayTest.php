<?php

include_once 'BaseTestCase.php';

use MultidimensionalArray\Dimension;
use MultidimensionalArray\MultidimensionalArray;
use PHPUnit\Framework\TestCase;

class MultidimensionalArrayTest extends BaseTestCase {

	public function testDefaultDimensionGenerator() {
		$o = $this->getTestArray();

		$this->assertSame(
			$this->getValidAnswer(),
			$o->by('prop')->items()
		);
	}

	public function testCallException() {
		$this->expectException(\BadMethodCallException::class);

		$o = $this->getTestArray();
		$o->invalidMethodName();
	}

	/**
	 * @depends testCallException
	 */
	public function testCall() {
		$o = $this->getTestArray();

		$this->assertSame(
			$this->getValidAnswer(),
			$o->byProp()->items()
		);
	}

	public function testAddDimension() {
		$testCase = $this->getCustomTestCase();

		$this->assertSame(7, $testCase->counter);
		$this->assertSame(
			$this->getValidAnswer(),
			$testCase->array->by('cstmProp')->items()
		);
		$this->assertSame(
			$this->getValidAnswer(),
			$testCase->array->byCstmProp()->items()
		);
		$this->assertSame(7, $testCase->counter);
	}

	public function testRemoveDimension() {
		$testCase = $this->getCustomTestCase();
		$testCase->counter = 0;
		$testCase->array->removeDimension('cstmProp');

		$testCase->array->recursive();
		$testCase->array->push(['prop' => 'prop6']);
		$testCase->array->shift();
		$testCase->array->shuffle();
		$this->assertSame(0, $testCase->counter);
		$this->assertSame(
			[],
			$testCase->dimension->array->items()
		);

		$testCase = $this->getCustomTestCase();
		$testCase->counter = 0;
		$testCase->array->removeDimension($testCase->dimension);

		$testCase->array->recursive();
		$testCase->array->push(['prop' => 'prop6']);
		$testCase->array->shift();
		$testCase->array->shuffle();
		$this->assertSame(0, $testCase->counter);
		$this->assertSame(
			[],
			$testCase->dimension->array->items()
		);
	}

	public function testHasDimension() {
		$testCase = $this->getCustomTestCase();

		$this->assertTrue($testCase->array->hasDimension('cstmProp'));
		$this->assertTrue($testCase->array->hasDimension(
			$testCase->dimension
		));
		$testCase->array->removeDimension('cstmProp');
		$this->assertFalse($testCase->array->hasDimension('cstmProp'));
		$this->assertFalse($testCase->array->hasDimension(
			$testCase->dimension
		));
	}

	protected function getCustomTestCase() {
		return new AddDimensionTestCase($this->getTestArray());
	}
}

class AddDimensionTestCase extends CustomGeneratorTestCase {

	public function __construct($array) {
		$this->array = $array;
		$this->dimension = new Dimension('cstmProp', function($k, $v) {
			$this->counter++;

			return $v['prop'];
		});
		$this->array->addDimension($this->dimension);
	}
}
