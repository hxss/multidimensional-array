<?php

namespace MultidimensionalArray;

use ArrayObject\ArrayObject;

/**
 * Extends ArrayObject and provides api for working with array Dimensions.
 */
class MultidimensionalArray extends ArrayObject {

	/**
	 * Array of dimensions.
	 * @var ArrayObject
	 */
	protected $dimensions = null;

	/**
	 * Constructs the class instance
	 * @param array $items array to interaction
	 * @param bool  $recursive (optional) Specify is array items of new object
	 * should be converted to ArrayObject too.
	 */
	public function __construct(array $items = [], $recursive = false) {
		parent::__construct(...func_get_args());

		$this->dimensions = new ArrayObject();
	}

	/**
	 * Checks if dimension registered for the array.
	 * @param  string|Dimension $dimension The dimension name or object.
	 * @return boolean
	 */
	public function hasDimension($dimension) {
		return is_string($dimension)
			? isset($this->dimensions[$dimension])
			: $this->dimensions->has($dimension);
	}

	/**
	 * Register new $dimension for the array.
	 * @param Dimension $dimension
	 * @return self
	 */
	public function addDimension(Dimension $dimension) {
		$this->dimensions[$dimension->name] = $dimension
			->setMaster($this);

		return $this;
	}

	/**
	 * Remove dimension from the array.
	 * @param  string|Dimension $dimension The dimension name or object.
	 * @return self
	 */
	public function removeDimension($dimension) {
		$name = is_string($dimension)
			? $dimension
			: $dimension->name;

		if (isset($this->dimensions[$name])) {
			$this->dimensions[$name]->unsetMaster();
			unset($this->dimensions[$name]);
		}

		return $this;
	}

	/**
	 * Returns associative array for dimension.
	 * @param  string $name Name of the dimension.
	 * @return MultidimensionalArray
	 */
	public function by($name) {
		if (!isset($this->dimensions[$name])) {
			$this->dimensions[$name] = Dimension::create($name)
				->setMaster($this);
		}

		return $this->dimensions[$name]->array;
	}

	/**
	 * Allows to call MultidimensionalArray::by method
	 * with camelCase dimensionName: byDimension();
	 * @return MultidimensionalArray
	 */
	public function __call($name, $args) {
		if (substr($name, 0, 2) === 'by') {
			return $this->by(lcfirst(substr($name, 2)));
		}

		throw new \BadMethodCallException(
			'Method ' . static::class . '::' . $name
				. 'doesn\'t exist.',
			1
		);

	}
}
