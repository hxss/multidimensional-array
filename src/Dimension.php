<?php

namespace MultidimensionalArray;

use ArrayObject\ArrayObject;
use ArrayObject\Traits\TraitSelfStaticFactory;
use MultidimensionalArray\MultidimensionalArray;
use Zend\EventManager\EventInterface;

/**
 * ArrayObject dimension.
 */
class Dimension {

	use TraitSelfStaticFactory;

	/**
	 * The dimension name.
	 * @var string
	 */
	public $name = '';
	/**
	 * Associative array representing dimension of the $master array.
	 * @var MultidimensionalArray
	 */
	public $array = null;

	/**
	 * Master array from which the dimension should be created.
	 * @var ArrayObject
	 */
	private $master = null;

	/**
	 * User-function that should take master array key & value
	 * and return new key for the dimension.
	 * @var callable
	 */
	private $keyGenerator = null;
	/**
	 * Array of collisions. Collision is are set of master array values
	 * that generates same key for the dimension.
	 * @var ArrayObject
	 */
	private $collisions = null;

	/**
	 * Flag for switching onSort trigger.
	 * If true the dimension array will be fully updated on every master-array sort.
	 * @var boolean
	 */
	private $updateOnSort = false;

	/**
	 * @param str           $name         Name of the dimension.
	 * @param callable|null $keyGenerator (optional) User-func for dimension key generation.
	 * @param boolean       $updateOnSort (optional) Flag for switching onSort trigger.
	 */
	public function __construct(
		$name,
		callable $keyGenerator = null,
		$updateOnSort = false
	) {
		$this->init();

		$this->name = $name;

		$this->keyGenerator = $keyGenerator
			?: $this->getDefaultKeyGenerator();

		$this->updateOnSort = $updateOnSort;
	}

	/**
	 * Sets master-array for the Dimension and run first dimension array generation.
	 * @param ArrayObject $master
	 * @return self
	 */
	public function setMaster(ArrayObject $master) {
		$this->unsetMaster();

		$this->master = $master;
		$this->attachListeners();
		$this->update();

		return $this;
	}

	/**
	 * Unsets master-array for the dimension and clears dimension array.
	 * @return self
	 */
	public function unsetMaster() {
		if ($this->master) {
			$this->detachListeners();
			$this->init();
			$this->master = null;
		}

		return $this;
	}

	/**
	 * Sets $updateOnSort flag new value and switches onSort trigers.
	 * @param boolean $update
	 * @return self
	 */
	public function setUpdateOnSort($update) {
		$prev = $this->updateOnSort;
		$this->updateOnSort = $update;

		$eventManager = $this->master->getEventManager();
		$prev != $this->updateOnSort
			&& (
				$this->updateOnSort
					? $eventManager->attach(
						ArrayObject::EVENT_SORT,
						[$this, 'update']
					) && $this->update()
					: $eventManager->detach(
						[$this, 'update'],
						ArrayObject::EVENT_SORT
					)
			);

		return $this;
	}

	/**
	 * Runs full dimension array update.
	 */
	public function update() {
		$this->init();
		foreach ($this->master as $key => $value) {
			$this->set($key, $value);
		}
	}

	/**
	 * Listener for ArrayObject::EVENT_ITEM_ADDED event.
	 * Adds new item to the dimension.
	 * @param EventInterface $e Master-array event.
	 */
	public function add(EventInterface $e) {
		$item = $e->getParams();
		$this->set($item->key, $item->value);
	}

	/**
	 * Listener for ArrayObject::EVENT_ITEM_REMOVED event.
	 * Removes(or replace with existing collision) item from the dimension.
	 * @param  EventInterface $e Master-array event.
	 */
	public function remove(EventInterface $e) {
		$item = $e->getParams();
		$key = $this->getKey($item->key, $item->value);

		if (
			isset($this->collisions[$key])
			&& $this->collisions[$key]->count()
		) {
			$value = $this->collisions[$key]->pop();
			$this->array[$key] = $value;
		} else {
			unset($this->array[$key]);
		}
	}

	/**
	 * Clears the dimension array and collisions.
	 */
	private function init() {
		$this->array = new MultidimensionalArray();
		$this->collisions = new ArrayObject();
	}

	/**
	 * Sets new item in the dimension array.
	 * @param mixed $key   Item key.
	 * @param mixed $value Item value.
	 */
	private function set($key, $value) {
		$newKey = $this->getKey($key, $value);

		if (isset($this->array[$newKey])) {
			if (!isset($this->collisions[$newKey])) {
				$this->collisions[$newKey] = new ArrayObject();
			}

			$this->collisions[$newKey]->push($this->array[$newKey]);
		}

		$this->array[$newKey] = $value;
	}

	/**
	 * Generates new key for the master-arrays item.
	 * @param mixed $key   Item key.
	 * @param mixed $value Item value.
	 * @return mixed       The dimension new Key.
	 */
	private function getKey($key, $value) {
		return call_user_func($this->keyGenerator, $key, $value);
	}

	/**
	 * Attaches listeneres to the master-arrays events.
	 */
	private function attachListeners() {
		$eventManager = $this->master->getEventManager();

		$eventManager->attach(
			ArrayObject::EVENT_ITEMS_UPDATED,
			[$this, 'update']
		);
		$eventManager->attach(
			ArrayObject::EVENT_ITEM_ADDED,
			[$this, 'add']
		);
		$eventManager->attach(
			ArrayObject::EVENT_ITEM_REMOVED,
			[$this, 'remove']
		);

		if ($this->updateOnSort) {
			$eventManager->attach(
				ArrayObject::EVENT_SORT,
				[$this, 'update']
			);
		}
	}

	/**
	 * Detaches listeneres from the master-arrays events.
	 */
	private function detachListeners() {
		$eventManager = $this->master->getEventManager();

		$eventManager->detach(
			[$this, 'update'],
			ArrayObject::EVENT_ITEMS_UPDATED
		);
		$eventManager->detach(
			[$this, 'add'],
			ArrayObject::EVENT_ITEM_ADDED
		);
		$eventManager->detach(
			[$this, 'remove'],
			ArrayObject::EVENT_ITEM_REMOVED
		);

		if ($this->updateOnSort) {
			$eventManager->detach(
				[$this, 'update'],
				ArrayObject::EVENT_SORT
			);
		}
	}

	/**
	 * Returns default key generator.
	 * Default generator tries to find new key value based on the dimensions name.
	 * @return callable
	 */
	private function getDefaultKeyGenerator() {
		return function ($key, $value) {
			$newKey = null;

			if (is_array($value) && isset($value[$this->name])) {
				$newKey = $value[$this->name];
			} elseif (isset($value->{$this->name})) {
				$newKey = $value->{$this->name};
			} elseif (method_exists($value, $this->name)) {
				$newKey = $value->{$this->name}();
			} elseif (method_exists(
				$value, $getter = 'get' . ucfirst($this->name))
			) {
				$newKey = $value->{$getter}();
			} else {
				throw new \Exception('Couldn\'t determine dimension key value.', 1);
			}

			return (string)$newKey;
		};
	}
}
